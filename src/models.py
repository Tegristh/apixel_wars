from typing import List, Optional
from sqlmodel import Field, Relationship, SQLModel
from datetime import datetime


class User(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    email: str = Field(index=True)
    username: str = Field(index=True)
    password: str
    is_active: bool = True 

    pixels: List["Pixel"] = Relationship(back_populates="user")

class UserConnect(SQLModel):
    username: str
    password: str

class Pixel(SQLModel, table=True):
    id: int = Field(primary_key=True)
    x_coord: int
    y_coord: int
    color: int
    last_modified_at: Optional[datetime] = Field(default= None)

    user_id: Optional[int] = Field(default=None, foreign_key="user.id")
    user: Optional[User] = Relationship(back_populates="pixels")

# class UserBase(SQLModel):
#     email: str = Field(index=True)
#     username: str = Field(index=True)
#     password: str

# class User(UserBase, table=True):
#     id: Optional[int] = Field(default=None, primary_key=True)
#     # is_active: Optional[bool] = Field(default=True) 
#     # is_admin: Optional[bool] = Field(default=False)
#     # hashed_password: Optional[str] = Field(default=None)

#     pixels: List["Pixel"] = Relationship(back_populates="user")


# class UserCreate(UserBase):
#     pass


# class UserRead(UserBase):
#     id: int

# class UserUpdate(SQLModel):
#     email: str | None = None
#     username: str | None = None



# class PixelBase(SQLModel):
#     x_coord: int
#     y_coord: int
#     color: int
#     last_modified_at: datetime

#     user_id: Optional[int] = Field(default=None, foreign_key="user.id")
    

# class Pixel(PixelBase, table=True):
#     id: int = Field(primary_key=True)

#     user: Optional[User] = Relationship(back_populates="pixels")

# class PixelCreate(PixelBase):
#     pass

# class PixelRead(PixelBase):
#     id:int

# class PixelUpdate(SQLModel):
#     Color: int | None = None
#     user_id: int | None = None
#     last_modified_at: datetime
