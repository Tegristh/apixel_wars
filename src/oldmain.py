from fastapi import FastAPI, status, Depends, Form
from fastapi.middleware.cors import CORSMiddleware
from fastapi.encoders import jsonable_encoder
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import jwt, JWTError
from passlib.context import CryptContext
from pydantic import BaseModel
from datetime import datetime, timedelta
from starlette.exceptions import HTTPException

app = FastAPI()
SECRET_KEY = "ipmesecretachangeravantmiseenprodfinale"
ALGORTITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    # allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

class Pixel(BaseModel):
    x_coord: int
    y_coord: int
    color: int
    last_modified_at: datetime = datetime.now()
    last_modified_by: int | None = None

class Token(BaseModel):
    access_token: str
    token_type: str

class TokenData(BaseModel):
    username: str

class User(BaseModel):
    username: str
    email: str
    disabled: bool = False


class UserInDB(User):
    hashed_password: str

class UserOut(User):
    pass

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)

def get_password_hash(password):
    return pwd_context.hash(password)

def get_user(db, username: str):
    if username in db:
        user_dict = db[username]
        return UserInDB(**user_dict)
    
def authenticate_user(fake_user_db, username: str, password: str):
    user = get_user(fake_user_db, username)
    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user

def create_access_token(data: dict, expires_delta: timedelta | None = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=30)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORTITHM)
    return encoded_jwt


fake_database = {}
fake_user_db = {}

@app.get("/")
async def hello_world():
    return {"message": "hello world - pouet"}

@app.post("/token", response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(fake_user_db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"}
        )
    access_token_expires = timedelta(minutes = ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires 
    )
    return {"access_token": access_token, "token_type": "Bearer"}

async def get_current_user(token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"}
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORTITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError:
        raise credentials_exception
    user = get_user(fake_user_db, username = token_data.username)
    if user is None:
        raise credentials_exception
    return user 

async def get_current_active_user(current_user: User = Depends(get_current_user)):
    if current_user.disabled:
        raise HTTPException(status_code=400, detail="Inactive User")
    return current_user

@app.get("/users/me", response_model=User)
async def get_me(current_user:User = Depends(get_current_active_user)):
    return current_user

@app.get("/users/")
async def get_users():
    return fake_user_db

@app.post("/register")
def registration(username: str = Form(...), email: str = Form(...), password: str = Form(...)):
    new_user = {}
    new_user["username"] = username
    new_user["email"] = email
    new_user["hashed_password"] = get_password_hash(password)

    print(new_user, new_user.username )
    fake_user_db[username] = new_user


@app.put("/pixels/{pixel_id}", response_model=Pixel)
def update_pixel(pixel_id: int, pixel: Pixel):
    update_pixel_encoded = jsonable_encoder(pixel)
    fake_database[pixel_id] = update_pixel_encoded
    return update_pixel_encoded

@app.get("/pixels/")
async def read_grid():
    return fake_database

@app.get("/pixel/{pixel_id}", response_model=Pixel)
async def read_pixel(pixel_id:int):
    return fake_database[pixel_id]