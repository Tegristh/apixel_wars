from typing import List
from datetime import datetime
from fastapi import FastAPI, Depends, Query, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from sqlmodel import Session, select, col

from passlib.context import CryptContext

from .database import create_db_and_tables, engine, get_session
from .models import User, UserConnect
# from .models import UserRead, UserBase, UserCreate, UserUpdate 
from .models import  Pixel
# from .models import PixelBase, PixelRead, PixelUpdate, PixelCreate

app = FastAPI()

SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES =  10080
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

def get_password_hash(password):
    return pwd_context.hash(password)

def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    # allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.on_event("startup")
def on_startup():
    create_db_and_tables()

@app.post("/login/")
def authenticate(
    *,
    session: Session = Depends(get_session), user: UserConnect,
):
    username = user.username
    db_user = session.exec(select(User).where(col(User.username) == username )).one()
    
    if (db_user is None):
        raise HTTPException(status_code=401, detail="Unauthorized")
    if not verify_password(user.password, db_user.password):
        raise HTTPException(status_code=401, details="Unauthorized")
    return db_user
    

@app.post("/users/", response_model=User)
def create_user(*, session: Session = Depends(get_session), user: User):
    hashed_password = get_password_hash(user.password)
    db_user = User.from_orm(user)
    db_user.password = hashed_password
    # print(db_user)
    session.add(db_user)
    session.commit()
    session.refresh(db_user)
    return db_user



@app.get("/users/", response_model=List[User])
def read_users(
    *, 
    session: Session = Depends(get_session),
    offset: int = 0,
    limit: int = Query(default=100, lte=100)
    ):
    users = session.exec(select(User).offset(offset).limit(limit)).all()
    return users

@app.get("/users/{user_id}", response_model=User)
def read_user(
    *,
    session: Session = Depends(get_session),
    user_id: int,
):
    user = session.get(User, user_id)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    return user

@app.put("/pixels/{pixel_id}", response_model=Pixel)
def update_pixel(
    *,
    session: Session = Depends(get_session),
    pixel: Pixel
):
    db_pixel= Pixel.from_orm(pixel)
    db_pixel.last_modified_at = datetime.now()
    # print(db_pixel)
    session.add(db_pixel)
    session.commit()
    session.refresh(db_pixel)
    return db_pixel

@app.get("/pixels/", response_model=List[Pixel])
def read_grid(
    *,
    session: Session = Depends(get_session),
):
    grid = session.exec(select(Pixel)).all()
    return grid