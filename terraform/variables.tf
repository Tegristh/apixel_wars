variable "resource_group_location" {
    type = string
    default = "West Europe"
    description = "Location for all resources."
}

variable "resource_group_name_prefix" {
    type = string
    default = "rg"
    description = "Prefix of the resource group name that's combined with a random value so name is unique in azure subscription"
}

variable "container_group_name_prefix" {
    type = string
    default = "acigroup"
    description = "prefix of the container group name that's combined with a random value so name is  unique in azure subscription"
}

variable "container_name_prefix" {
    type = string
    description = "prefix of the container name that's combined with a random value so name is unique in azure subscription"
    default = "aci"
}

variable "image" {
    type = string
    description = "container image to deploy."
    default = "registry.gitlab.com/tegristh/apixel_wars"
}

variable "port" {
    type = number
    description = "Port to open on the container and the public IP adress."
    default = 80
}

variable "cpu_cores" {
    type = number
    description = "The number of CPU cores to allocate to the container."
    default = 0.5
}

variable "memory_in_gb" {
    type = number
    description = "The amount of memory to allocate to the container in gigabytes."
    default = 0.5
}

variable "restart_policy" {
    type = string
    description = "The behavior of Azure runtime if container has stopped."
    default = "Always"
    validation {
        condition = contains(["Always", "Never", "Onfailure"], var.restart_policy)
        error_message = "The restart_policy must be one ot the following: Always, Never, Onfailure."
    }
}